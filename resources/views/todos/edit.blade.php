@extends('layouts.app')
@section('content')

<h1>create new todo</h1>
<form method  = 'post' action = "{{action('TodoController@update',$todo->id)}}">
@csrf
@method('PATCH')
<div>
    <label for = "title"> Update your task: </label>
    <input type ="text" class = "form-control" name = "title" value= "{{$todo->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name = "submit" value="updete">
</div>


</form>


<form method  = 'post' action = "{{action('TodoController@destroy',$todo->id)}}">
@csrf
@method('DELETE')

<div class = "form-group">
    <input type ="submit" class = "form-control" name = "submit" value="Delete">
</div>


</form>
@endsection
