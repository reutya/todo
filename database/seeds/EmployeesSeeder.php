<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('users')->insert(
                
                
            [
                   [
                    'name' => 'daniella',
                    'email' => 'dk@gmail.com',
                    'password'=> Hash::make('12345678'),
                    'role'=>'employee',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
    
                [
                    'name' => 'kerem',
                    'email' => 'k@gmail.com',
                    'password'=> Hash::make('12345678'),
                    'role'=>'employee',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
    
    
               
            ]);
        }
    }
}





