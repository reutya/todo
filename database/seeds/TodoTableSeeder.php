<?php

use Illuminate\Database\Seeder;

class TodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert(
            
            
        [
               [
            'title' => 'buy milk',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            ],

            [
                'title' => 'study test',
                'user_id' => 2,
                'created_at' => date('Y-m-d G:i:s'),
            ],


            [
                'title' => 'study test',
                'user_id' => 2,
                'created_at ' => date('Y-m-d G:i:s'),
            ]
        ]);
    }
}
